import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df = df[['Name','Age']]
    text =  ["Mr.", "Mrs.", "Miss."]

    df_1 = df.loc[df['Name'].str.contains('Mr.', na=False)]
    count_1 = df_1.isnull().sum()
    median_1 = df_1['Age'].median()

    df_2 = df.loc[df['Name'].str.contains('Mrs.',na=False)]
    count_2 = df_2.isnull().sum()
    median_2 = df_2['Age'].median()

    df_3 = df.loc[df['Name'].str.contains('Miss.',na=False)]
    count_3 = df_3.isnull().sum()
    median_3 = df_3['Age'].median()

    
    return [(text[0],count_1, median_1),(text[1],count_2, median_2),
        (text[2],count_3, median_3)]
